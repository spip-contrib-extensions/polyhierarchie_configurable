# Polyhiérarchie configurable

Le plugin Polyhiérarchie sait techniquement gérer tous les objets, mais ne propose les rubriques indirectes que sur les articles et les rubriques.

Ce plugin permet de configurer tous les objets où on veut pouvoir lier des rubriques indirectes avec le même principe.

Ces objets sont alors reconnus comme des enfants de la rubrique (et normalement la rendent publiée).
