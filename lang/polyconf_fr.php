<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;
 
$GLOBALS[$GLOBALS['idx_lang']] = array(
	// C
	'configurer_lier_objets_label' => 'Activer le classement par rubrique sur ces contenus',
	'configurer_titre' => 'Configurer le classement par rubriques',
	
	// P
	'polyconf_titre' => 'Polyhiérarchie configurable',
);

?>
