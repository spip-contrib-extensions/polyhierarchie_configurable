<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'polyconf_description' => 'Ce plugin ajoute des configurations supplémentaires au plugin Polyhiérarchie, pour permettre de classer tout type de contenu avec des rubriques.',
	'polyconf_nom' => 'Polyhiérarchie configurable',
	'polyconf_slogan' => 'Plus de configuration pour classer vos contenus avec des rubriques',
);

?>