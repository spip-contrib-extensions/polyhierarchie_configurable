<?php
/**
 * Définit les autorisations du plugin Polyhiérarchie configurable
 *
 * @plugin     Polyhiérarchie configurable
 * @copyright  2013
 * @author     Les Développements Durables
 * @licence    GNU/GPL v3
 * @package    SPIP\Polyconf\Autorisations
 */

if (!defined('_ECRIRE_INC_VERSION')) return;


/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser */
function polyconf_autoriser(){}



?>
