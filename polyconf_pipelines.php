<?php
/**
 * Utilisations de pipelines par Polyhiérarchie configurable
 *
 * @plugin     Polyhiérarchie configurable
 * @copyright  2013
 * @author     Les Développements Durables
 * @licence    GNU/GPL v3
 * @package    SPIP\Polyconf\Pipelines
 */

if (!defined('_ECRIRE_INC_VERSION')) return;
	
/**
 * Utilisation du pipeline afficher_contenu_objet
 * 
 * - Insertion d'un sélecteur de rubriques dans les objets configurés pour ça
 *
 * @pipeline afficher_contenu_objet
 * 
 * @param array $flux
 *     Données du pipeline
 * @return array
 *     Données du pipeline
 */
function polyconf_afficher_complement_objet($flux) {
	include_spip('inc/config');
	
	// Ajouter un bloc de liaison avec les rubriques sur les objets configurés pour ça
	if (
		$table = table_objet_sql($flux['args']['type'])
		and in_array($table, lire_config('polyhier/lier_objets', array()))
	) {
		$id = $flux['args']['id'];
		$infos = recuperer_fond('prive/objets/editer/polyhierarchie', array(
			'objet'=>$flux['args']['type'],
			'id_objet'=>$id,
			'editable'=>autoriser('associerrubrique', $flux['args']['type'], $id) ? 'oui':'non'
		));
		$flux['data'] .= $infos;
	}
	
	return $flux;
}

/**
 * Compter les enfants indirects d'une rubrique
 *
 * @param array $flux
 * @return array
 */
function polyconf_objet_compte_enfants($flux) {
	if ($flux['args']['objet'] == 'rubrique' and $liaisons = lire_config('polyhier/lier_objets', [])) {
		// Pour chaque objet configuré comme pouvant avoir des rubriques indirectes
		foreach ($liaisons as $table) {
			$objet = objet_type($table);
			$objets = table_objet($table);
			$cle = id_table_objet($table);
			$statut = (isset($flux['args']['statut']) ? ' AND O.statut=' . sql_quote($flux['args']['statut']) : '');
			$postdates = ($GLOBALS['meta']['post_dates'] == 'non') ? ' AND A.date <= ' . sql_quote(date('Y-m-d H:i:s')) : '';
			
			if (!isset($flux['data']["{$objets}_indirect"])) {
				$flux['data']["{$objets}_indirect"] = 0;
			}
			
			$flux['data']["{$objets}_indirect"] += sql_countsel(
				"spip_rubriques_liens as RL join $table as A ON (RL.objet='$objet' AND RL.id_objet=A.$cle)",
				'RL.id_parent=' . $flux['args']['id_objet'] . $statut . $postdates
			);
		}
	}
	
	return $flux;
}


function polyconf_calculer_rubriques($flux) {
	if ($liaisons = lire_config('polyhier/lier_objets', [])) {
		$tables_objets = lister_tables_objets_sql();
		
		// Pour chaque objet configuré comme pouvant avoir des rubriques indirectes
		foreach ($liaisons as $table) {
			$objet = objet_type($table);
			$objets = table_objet($table);
			$cle = id_table_objet($table);
			
			// On vérifie si l'objet a un statut et une date
			$where = [];
			if (isset($tables_objets[$table]['statut'][0])) {
				$champ_statut = $tables_objets[$table]['statut'][0]['champ'];
				$valeurs = explode(',', $tables_objets[$table]['statut'][0]['publie']);
				$where[] = "fille.$champ_statut IN ('" . implode("', '", $valeurs) . "')";
			}
			if (isset($tables_objets[$table]['date'])) {
				$champ_date = $tables_objets[$table]['date'];
				$where[] = "rub.date_tmp <= fille.$champ_date";
			}
			$where = implode(' and ', $where);
			
			$r = sql_select(
				'rub.id_rubrique AS id, max(fille.date) AS date_h',
				"spip_rubriques AS rub
								JOIN spip_rubriques_liens as RL ON rub.id_rubrique = RL.id_parent
								JOIN $table as fille ON (RL.objet='$objet' AND RL.id_objet=fille.$cle)",
				$where,
				'rub.id_rubrique'
			);
			while ($row = sql_fetch($r)) {
				sql_updateq('spip_rubriques', ['statut_tmp' => 'publie', 'date_tmp' => $row['date_h']], 'id_rubrique=' . $row['id']);
			}
		}
	}

	return $flux;
}
